#!/bin/bash

TARGET=$HOME/.local/bin

case $OSTYPE in

  linux-gnu)
    PLATFORM=`arch`-unknown-$OSTYPE
    ;;

  *)
    echo $0 does not support $OSTYPE >&2
    exit 1
    ;;

esac

REVISION=`wget https://github.com/rust-lang/rust-analyzer/releases.atom -O - 2> /dev/null | grep '<link' | grep github | grep -e '20[0-9][0-9]' | awk -e 'match($0, /([[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2})/, m) { print m[1] }' | head -n1`
PATHNAME=rust-analyzer-$PLATFORM.gz
URL=https://github.com/rust-lang/rust-analyzer/releases/download/$REVISION/$PATHNAME
wget $URL -O - | gzip -cd > $TARGET/rust-analyzer && chmod 755 $TARGET/rust-analyzer
